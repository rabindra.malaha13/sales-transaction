import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// angular material
import { MatToolbarModule } from  '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from "@angular/material/divider";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatRippleModule } from "@angular/material/core";
import { MatSelectModule } from "@angular/material/select";

//application component
import { CustomerComponent } from './setup/customer/customer.component';
import { ProductComponent } from './setup/product/product.component';
import { SaleTrasnactionComponent } from './setup/sale-transaction/sale-trasnaction.component';

import { ReactiveFormsModule } from '@angular/forms';

// Kendo grid module
import { GridModule } from '@progress/kendo-angular-grid';

//http routing
import { HttpClientModule } from '@angular/common/http';
import { AddCustomerComponent } from './setup/customer/add-customers/add-customer/add-customer.component';
import { AddProductComponent } from './setup/product/add-products/add-product/add-product.component';
import { AddSaleTransactionComponent } from './setup/sale-transaction/add-sale-transactions/add-sale-transaction/add-sale-transaction.component';
import { InvoiceComponent } from './setup/invoices/invoice/invoice.component';

import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
    CustomerComponent,
    ProductComponent,
    SaleTrasnactionComponent,
    AddCustomerComponent,
    AddProductComponent,
    AddSaleTransactionComponent,
    InvoiceComponent
  ],
  imports: [
    GridModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatDividerModule,
    MatMenuModule,
    MatSelectModule,
    HttpClientModule,
    GridModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
