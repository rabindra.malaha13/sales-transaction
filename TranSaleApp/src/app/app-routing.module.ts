import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SaleTrasnactionComponent } from './setup/sale-transaction/sale-trasnaction.component';
import { ProductComponent } from './setup/product/product.component';
import { CustomerComponent } from './setup/customer/customer.component';

const routes: Routes = [
  { path: '', redirectTo: '/sale-transactions', pathMatch: 'full' },
  { path: 'sale-transactions', component: SaleTrasnactionComponent },
  { path: 'products', component: ProductComponent },
  { path: 'customers', component: CustomerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponent = [
  SaleTrasnactionComponent, ProductComponent, CustomerComponent
]