export interface ProductDto {
    id: number,
    name: string,
    brandName: string,
    price: number,
    description: string
}

export interface ProductCreateDto {
    name: string,
    brandName: string,
    price: number,
    description: string
}

export interface ProductUpdateDto {
    name: string,
    brandName: string,
    price: number,
    description: string
}