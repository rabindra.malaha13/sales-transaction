import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConstantSettings } from 'src/app/constants/app-constant-settings';
import { ProductCreateDto, ProductUpdateDto } from './product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  api: string = AppConstantSettings.API_ENDPOINT + "products";
  constructor(private httpClient: HttpClient) { }

  public getAll(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.api);
  }

  public getById(id: number){
    return this.httpClient.get(this.api + "/" + id);
  }

  public create(input: ProductCreateDto){
    return this.httpClient.post(this.api, input)
  }

  public update(id: number, input: ProductUpdateDto){
    return this.httpClient.put(this.api + "/" + id, input)
  }

  public delete(id: number){
    return this.httpClient.delete(this.api + "/" + id);
  }

  public getSelectListItem(): Observable<any>{
    return this.httpClient.get<any>(this.api + "/get-select-items");
  }
}
