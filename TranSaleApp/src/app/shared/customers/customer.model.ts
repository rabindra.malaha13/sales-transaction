export interface CustomerDto {
    id: number,
    name: string,
    email: string,
    contactNumber: string,
    address: string
}

export interface CustomerCreateDto{
    name: string,
    email: string,
    contactNumber: string,
    address: string
}

export interface CustomerUpdateDto{
    name: string,
    email: string,
    contactNumber: string,
    address: string
}
