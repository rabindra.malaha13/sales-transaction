import { Injectable } from '@angular/core';
import { CustomerCreateDto, CustomerDto, CustomerUpdateDto } from './customer.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConstantSettings } from 'src/app/constants/app-constant-settings';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }
  api: string = AppConstantSettings.API_ENDPOINT + "customers";
  public getAll(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.api);
  }

  public getById(id: number): Observable<any>{
    return this.httpClient.get<any>(this.api + "/" + id);
  }

  public create(input: CustomerCreateDto): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.post(this.api, input, httpOptions)
  }

  public update(id: number, input: CustomerUpdateDto): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.put(this.api + "/" + id, input, httpOptions)
  }

  public delete(id: number){
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.delete(this.api + "/" + id, httpOptions);
  }

  public getSelectListItem(): Observable<any>{
    return this.httpClient.get<any>(this.api + "/get-select-items");
  }
}
