import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstantSettings } from 'src/app/constants/app-constant-settings';
import { InvoiceCreateDto, SaleTransactionCreateDto, SaleTransactionUpdateDto } from './sale-transaction.model';

@Injectable({
  providedIn: 'root'
})
export class SaleTransactionService {

  constructor(private httpClient: HttpClient) { }
  api: string = AppConstantSettings.API_ENDPOINT + "sale-transactions";
  public getAll(): Observable<any[]>{
    return this.httpClient.get<any[]>(this.api);
  }

  public getAllForInvoice(invoiceId: number, customerId: number): Observable<any[]>{
    return this.httpClient.get<any[]>(this.api + "/get-sale-transaction-for-invoice" + "/" + invoiceId + "/" + customerId);
  }

  public getById(id: number): Observable<any>{
    return this.httpClient.get<any>(this.api + "/" + id);
  }

  public create(input: SaleTransactionCreateDto): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.post(this.api, input, httpOptions);
  }

  public update(id: number, input: SaleTransactionUpdateDto): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.put(this.api + "/" + id, input, httpOptions)
  }

  public delete(id: number){
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.delete(this.api + "/" + id, httpOptions);
  }

  public generateInvoice(input: InvoiceCreateDto): Observable<any>{
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.post(this.api + "/generate-invoice", input, httpOptions);
  }
}
