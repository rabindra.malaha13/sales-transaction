export interface SaleTransactionDto {
    id: number,
    customerId: number,
    customerName: string,
    productName: string,
    productId: number,
    rate: number,
    quantity: number,
    totalAmount: number,
    invoiceId: number,
    isChecked: boolean
}

export interface SaleTransactionCreateDto{
    customerId: number,
    productId: number,
    rate: number,
    quantity: number,
    totalAmount: number
}

export interface SaleTransactionUpdateDto{
    id: number,
    customerId: number,
    customerName: string,
    productName: string,
    productId: number,
    rate: number,
    quantity: number,
    totalAmount: number
}

export interface InvoiceCreateDto{
    customerId: number,
    totalAmount: number,
    saleTransactions: SaleTransactionDto[]
}
