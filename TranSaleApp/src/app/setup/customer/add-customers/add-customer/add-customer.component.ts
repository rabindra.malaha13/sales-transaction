import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CustomerCreateDto, CustomerUpdateDto } from 'src/app/shared/customers/customer.model';
import { CustomerService } from 'src/app/shared/customers/customer.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent {
  constructor(
    private toastr: ToastrService,
    private service: CustomerService, 
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) 
    {
      // this.buildForm();
    }
  input = {} as CustomerCreateDto;
  inputUpdate = {} as CustomerUpdateDto;
  public breakpoint: number; // Breakpoint observer code
  public form: FormGroup;
  isFormShown: boolean;
  id: number;
  title: string = "Add";

  ngOnInit(): void{
    if(this.data){
      this.input = this.dialogRef.componentInstance.data;
      this.id = this.dialogRef.componentInstance.data.id;
      this.title = "Update";
    }
    this.buildForm();
  }

  buildForm(){
    const {
      name,
      email,
      contactNumber,
      address
    } = this.input || {};

    this.form = this.fb.group({
      name: [name ?? null, [Validators.required]],
      email: [email ?? null, [Validators.required]],
      contactNumber: [contactNumber ?? null, [Validators.required]],
      address: [address ?? null, [Validators.required]]
    });

    // this.breakpoint = window.innerWidth <= 600 ? 1 : 2; 
    // Breakpoint observer code
  }


  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  close() {
    this.dialogRef.close();
}

submit(){
  if(this.id){
    this.updateCustomer();
  }
  else{
    this.createCustomer();
  }
}

createCustomer(){
  if(this.form.invalid) return;
  this.service.create(this.form.value).subscribe((res) =>{
    if(res){
      this.close();
      this.toastr.success("Customer created successfully", "Success");
    }
  })
}
updateCustomer(){
  this.service.update(this.id, this.form.value).subscribe((res) =>{
    if(res){
      this.close();
      this.toastr.success("Customer updated successfully", "Success");
    }
  });
}
}
