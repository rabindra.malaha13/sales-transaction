import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerCreateDto, CustomerDto, CustomerUpdateDto } from 'src/app/shared/customers/customer.model';
import { CustomerService } from 'src/app/shared/customers/customer.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material/dialog';
import { AddCustomerComponent } from './add-customers/add-customer/add-customer.component';
import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { process } from "@progress/kendo-data-query";
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent {
  constructor(
    private service: CustomerService, 
    private fb: FormBuilder,
    private dialog: MatDialog) { }

  customers: CustomerDto[] = [];
  id: number = 0;
  input = {} as any;
  inputUpdate = {} as CustomerUpdateDto;
  public mySelection: string[] = [];
  form: FormGroup;
  isFormShown: boolean;

  @ViewChild(DataBindingDirective) dataBinding!: DataBindingDirective;
  public gridData: unknown[] = this.customers;
  public gridView!: unknown[];
  
  ngOnInit(): void{
    this.getAllCustomers();
  }

  public onFilter(input: Event): void {
    const inputValue = (input.target as HTMLInputElement).value;

    this.gridView = process(this.gridData, {
      filter: {
        logic: "or",
        filters: [
          {
            field: "name",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "email",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "contactNumber",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "address",
            operator: "contains",
            value: inputValue,
          },
        ],
      },
    }).data;

    this.dataBinding.skip = 0;
  }

  buildForm(){
    const {
      name,
      email,
      contactNumber,
      address
    } = this.input;

    this.form = this.fb.group({
      name: [name ?? null, [Validators.required]],
      email: [email ?? null, [Validators.required], [Validators.email]],
      contactNumber: [contactNumber ?? null, [Validators.required]],
      address: [address ?? null, [Validators.required]]
    });
  }

  editForm(event : any){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = event.dataItem;
    console.log(event.dataItem);
    this.dialog.open(AddCustomerComponent, dialogConfig)
      .afterClosed().subscribe(() =>{
        this.getAllCustomers();
      })
  }

  showForm() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(AddCustomerComponent, dialogConfig)
      .afterClosed().subscribe(() =>{
        this.getAllCustomers();
      })
}

  closeForm(){
    this.isFormShown = false;
    this.form.reset();
  }

  getAllCustomers(){
    this.service.getAll().subscribe((res) =>{
      this.customers = res;
      console.log(this.customers)
    })
  }

  submit(){
    if(this.form.invalid) return;
    if(this.id){
      this.updateProduct();
    }
    else{
      this.createProduct();
    }
  }

  createProduct(){
    this.service.create(this.form.value).subscribe((res) =>{
      if(res){
        this.closeForm();
        this.getAllCustomers();
      }
    })
  }

  updateProduct(){
    this.service.update(this.id, this.form.value).subscribe((res) =>{
      if(res){
        this.closeForm();
        this.getAllCustomers();
      }
    });
  }

  deleteProduct(){
    this.service.delete(this.id).subscribe((res) =>{
      if(res){
        this.closeForm();
        this.getAllCustomers();
      }
    });
  }

  getProductById(){
    this.service.getById(this.id).subscribe();
  }
}
