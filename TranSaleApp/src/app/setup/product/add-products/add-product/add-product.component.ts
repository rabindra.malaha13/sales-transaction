import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ProductCreateDto, ProductUpdateDto } from 'src/app/shared/products/product.model';
import { ProductService } from 'src/app/shared/products/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent {
  constructor(
    private toastr: ToastrService, 
    private service: ProductService, 
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any)  { }

    input = {} as ProductCreateDto;
    inputUpdate = {} as ProductUpdateDto;
    public breakpoint: number; // Breakpoint observer code
    public form: FormGroup;
    isFormShown: boolean;
    id: number;
    title: string = "Add";

    ngOnInit(): void{
      if(this.data){
        this.input = this.dialogRef.componentInstance.data;
        this.id = this.dialogRef.componentInstance.data.id;
        this.title = "Update";
      }
      this.buildForm();
    }

    buildForm(){
      const {
        name,
        brandName,
        price,
        description
      } = this.input || {};
  
      this.form = this.fb.group({
        name: [name ?? null, [Validators.required]],
        brandName: [brandName ?? null, [Validators.required]],
        price: [price ?? null, [Validators.required]],
        description: [description ?? null, [Validators.required]]
      });
  
      // this.breakpoint = window.innerWidth <= 600 ? 1 : 2; 
      // Breakpoint observer code
    }

    
  public onResize(event: any): void {
    this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
  }

  close() {
    this.dialogRef.close();
}
submit(){
  if(this.id){
    this.updateProduct();
  }
  else{
    this.createProduct();
  }
}

createProduct(){
  if(this.form.invalid) return;
  this.service.create(this.form.value).subscribe((res) =>{
    if(res){
      this.close();
      this.toastr.success("Product created successfully!", "Success");
    }
  })
}
updateProduct(){
  this.service.update(this.id, this.form.value).subscribe((res) =>{
    if(res){
      this.close();
      this.toastr.success("Product created successfully!", "Success");
    }
  });
}
}
