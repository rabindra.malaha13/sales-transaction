import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { ProductDto, ProductUpdateDto } from 'src/app/shared/products/product.model';
import { ProductService } from 'src/app/shared/products/product.service';
import { process } from "@progress/kendo-data-query";
import { AddProductComponent } from './add-products/add-product/add-product.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  constructor(
    private service: ProductService, 
    private fb: FormBuilder,
    private dialog: MatDialog) { }

    products: ProductDto[] = [];
    id: number = 0;
    input = {} as any;
    inputUpdate = {} as ProductUpdateDto;
    public mySelection: string[] = [];
    form: FormGroup;
    isFormShown: boolean;

    @ViewChild(DataBindingDirective) dataBinding!: DataBindingDirective;
    public gridData: unknown[] = this.products;
    public gridView!: unknown[];

    ngOnInit(): void{
      this.getAllProducts();
    }

    public onFilter(input: Event): void {
      const inputValue = (input.target as HTMLInputElement).value;
  
      this.gridView = process(this.gridData, {
        filter: {
          logic: "or",
          filters: [
            {
              field: "name",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "brandName",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "price",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "description",
              operator: "contains",
              value: inputValue,
            },
          ],
        },
      }).data;
  
      this.dataBinding.skip = 0;
    }

    getAllProducts(){
      this.service.getAll().subscribe((res) =>{
        this.products = res;
      })
    }

    editForm(event : any){
      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = event.dataItem;
      console.log(event.dataItem);
      this.dialog.open(AddProductComponent, dialogConfig)
        .afterClosed().subscribe(() =>{
          this.getAllProducts();
        });
    }
  
    showForm() {
      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      this.dialog.open(AddProductComponent, dialogConfig)
        .afterClosed().subscribe(() =>{
          this.getAllProducts();
        });
  }
}
