import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleTrasnactionComponent } from './sale-trasnaction.component';

describe('SaleTrasnactionComponent', () => {
  let component: SaleTrasnactionComponent;
  let fixture: ComponentFixture<SaleTrasnactionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SaleTrasnactionComponent]
    });
    fixture = TestBed.createComponent(SaleTrasnactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
