import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from 'src/app/shared/customers/customer.service';
import { ProductService } from 'src/app/shared/products/product.service';
import { SaleTransactionCreateDto, SaleTransactionUpdateDto } from 'src/app/shared/sale-transactions/sale-transaction.model';
import { SaleTransactionService } from 'src/app/shared/sale-transactions/sale-transaction.service';

@Component({
  selector: 'app-add-sale-transaction',
  templateUrl: './add-sale-transaction.component.html',
  styleUrls: ['./add-sale-transaction.component.css']
})
export class AddSaleTransactionComponent {
  constructor(
    private toastr: ToastrService,
    private service: SaleTransactionService, 
    private customerService: CustomerService,
    private productService: ProductService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddSaleTransactionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any)  { }

    input = {} as SaleTransactionCreateDto;
    inputUpdate = {} as SaleTransactionUpdateDto;
    public breakpoint: number; // Breakpoint observer code
    public form: FormGroup;
    isFormShown: boolean;
    id: number;
    title: string = "Add";

    customerSelectItems: any[] = [];
    productSelectItems: any[] = [];

    ngOnInit(): void{
      if(this.data){
        this.input = this.dialogRef.componentInstance.data;
        this.id = this.dialogRef.componentInstance.data.id;
        this.title = "Update";
        console.log(this.id)
      }
      this.getCustomerSelectItems();
      this.getProductSelectItems();
      this.buildForm();
    }
    
    buildForm(){
      const {
        customerId,
        productId,
        rate,
        quantity,
        totalAmount
      } = this.input || {};
  
      this.form = this.fb.group({
        customerId: [customerId ?? null, [Validators.required]],
        productId: [productId ?? null, [Validators.required]],
        rate: [rate ?? null, [Validators.required]],
        quantity: [quantity ?? null, [Validators.required]],
        totalAmount: [totalAmount ?? null, [Validators.required]]
      });
  
      // this.breakpoint = window.innerWidth <= 600 ? 1 : 2; 
      // Breakpoint observer code
    }

    public onResize(event: any): void {
      this.breakpoint = event.target.innerWidth <= 600 ? 1 : 2;
    }

    onProductChange(event: any){
      this.productSelectItems.forEach(data => {
        if(event.value == data.id){
          this.form.patchValue({ rate: data.price });
        }
      })

      if(this.form.value.quantity){
        this.form.patchValue({
          totalAmount: this.form.value.rate * this.form.value.quantity
        }) 
      }
    }

    onQuantityChange(event: any){
      console.log(event);
      this.form.patchValue({
        totalAmount: this.form.value.rate * this.form.value.quantity
      })
    }
  
    close() {
      this.dialogRef.close();
    }

    submit(){
      if(this.id){
        this.updateTransaction();
      }
      else{
        this.createTransaction();
      }
    }

    getProductSelectItems(){
      this.productService.getSelectListItem().subscribe((res) =>{
        this.productSelectItems = res;
      })
    }

    getCustomerSelectItems(){
      this.customerService.getSelectListItem().subscribe((res) =>{
        this.customerSelectItems = res;
      })
    }
    
    createTransaction(){
      if(this.form.invalid) return;
      this.service.create(this.form.value).subscribe((res) =>{
        if(res){
          this.close();
          this.toastr.success("Sale created successfully!", "Success");
        }
      })
    }
    updateTransaction(){
      this.service.update(this.id, this.form.value).subscribe((res) =>{
        if(res){
          this.close();
          this.toastr.success("Sale updated successfully!", "Success");
        }
      });
    }
}
