import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { SaleTransactionDto, SaleTransactionUpdateDto } from 'src/app/shared/sale-transactions/sale-transaction.model';
import { SaleTransactionService } from 'src/app/shared/sale-transactions/sale-transaction.service';
import { process } from "@progress/kendo-data-query";
import { AddSaleTransactionComponent } from './add-sale-transactions/add-sale-transaction/add-sale-transaction.component';
import { InvoiceComponent } from '../invoices/invoice/invoice.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sale-trasnaction',
  templateUrl: './sale-trasnaction.component.html',
  styleUrls: ['./sale-trasnaction.component.css']
})
export class SaleTrasnactionComponent {
  constructor(
    private toastr: ToastrService,
    private service: SaleTransactionService, 
    private fb: FormBuilder,
    private dialog: MatDialog) { }

    transactions: SaleTransactionDto[] = [];
    isUnchecked: boolean = false;
    transactionForInvoice: SaleTransactionDto[] = [];
    id: number = 0;
    input = {} as any;
    inputUpdate = {} as SaleTransactionUpdateDto;
    public mySelection: string[] = [];
    form: FormGroup;
    isFormShown: boolean;
    unCheck: boolean;
    isAnotherCustomer: boolean;

    @ViewChild(DataBindingDirective) dataBinding!: DataBindingDirective;
    public gridData: unknown[] = this.transactions;
    public gridView!: unknown[];

    ngOnInit(): void{
      this.getAllTransactions();
      this.unCheck = true;
    }

    public onFilter(input: Event): void {
      const inputValue = (input.target as HTMLInputElement).value;
  
      this.gridView = process(this.gridData, {
        filter: {
          logic: "or",
          filters: [
            {
              field: "name",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "brandName",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "price",
              operator: "contains",
              value: inputValue,
            },
            {
              field: "description",
              operator: "contains",
              value: inputValue,
            },
          ],
        },
      }).data;
  
      this.dataBinding.skip = 0;
    }

    toggle(dataItem: any){
      if(!dataItem.isChecked){
        dataItem.isChecked = true;
      if(this.transactionForInvoice.length === 0){
        this.transactionForInvoice.push(dataItem);
      }
      else{
        this.transactionForInvoice.forEach((data, index) =>{
          if(dataItem.id === data.id){
            this.isUnchecked = true;
            this.transactionForInvoice.splice(index, 1)
          }
          if(dataItem.customerId != data.customerId){
            this.isAnotherCustomer = true;
            dataItem.isChecked = false;
          }
        })
        if(!this.isUnchecked){
          if(this.isAnotherCustomer){
            this.toastr.warning("Choose sales for same customer.","Warning");
            this.isAnotherCustomer = false;
          }
          else{
            this.transactionForInvoice.push(dataItem);
          }
        }
        else{
          this.isUnchecked = false;
        }
      }
    }
      console.log(this.transactionForInvoice)
    }

    getAllTransactions(){
      this.service.getAll().subscribe((res) =>{
        this.transactions = res;
      })
    }

    editForm(event : any){
      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = event.dataItem;
      console.log(event.dataItem);
      this.dialog.open(AddSaleTransactionComponent, dialogConfig)
        .afterClosed().subscribe((res) =>{
          this.getAllTransactions();
        });
    }
  
    showForm() {
      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      this.dialog.open(AddSaleTransactionComponent, dialogConfig)
        .afterClosed().subscribe(() =>{
          this.getAllTransactions();
          this.unCheck = true;
        });
    }

    showInvoiceForm(transactions: any[]){
      const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = transactions;
      this.dialog.open(InvoiceComponent, dialogConfig)
        .afterClosed().subscribe(() =>{
          this.getAllTransactions();
          this.transactionForInvoice = [];
          this.unCheck = true;
        });
    }

    showInvoice(invoiceId: number, customerId: number){
      this.service.getAllForInvoice(invoiceId, customerId).subscribe((res) =>{
        this.transactionForInvoice = res;
        this.showInvoiceForm(this.transactionForInvoice);
      })
    }
}
