import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { InvoiceCreateDto, SaleTransactionDto } from 'src/app/shared/sale-transactions/sale-transaction.model';
import { SaleTransactionService } from 'src/app/shared/sale-transactions/sale-transaction.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent {
  constructor(
    private toastr: ToastrService,
    private saleService: SaleTransactionService, 
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<InvoiceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any)  { }

    transactionForInvoice: any[] = [];
    title: string = "Add";
    customerId: number;
    totalAmount: number = 0;
    input = {} as InvoiceCreateDto;
    form: FormGroup;

    disableSubmitButton: boolean = false;

    ngOnInit(){
      this.transactionForInvoice = this.dialogRef.componentInstance.data;
      if(this.transactionForInvoice.length > 0){
        this.customerId = this.transactionForInvoice[0].customerId;
        this.transactionForInvoice.forEach((res) =>{
          this.totalAmount += res.totalAmount;
        })
        if(this.transactionForInvoice[0].invoiceId){
          this.disableSubmitButton = true;
        }
      }
      else{
        this.disableSubmitButton = true;
      }
    }

    close(){
      this.disableSubmitButton = false;
      this.dialogRef.close({event: 'Cancel'});
    }

    submit(){
      this.input.customerId = this.customerId;
      this.input.totalAmount = this.totalAmount;
      this.input.saleTransactions = this.transactionForInvoice;
      console.log(this.input);
      this.saleService.generateInvoice(this.input).subscribe((res) =>{
         if(res){
            this.close();
            this.toastr.success("Invoice generated successfully", "Success");
         }
      })
    }

}
