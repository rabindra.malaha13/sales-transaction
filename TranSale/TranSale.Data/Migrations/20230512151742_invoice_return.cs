﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TranSale.Data.Migrations
{
    /// <inheritdoc />
    public partial class invoice_return : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InvoiceReturns",
                columns: table => new
                {
                    ReturnId = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvoiceReturns");
        }
    }
}
