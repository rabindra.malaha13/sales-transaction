USE [SaleTransactionLat]
GO
/****** Object:  StoredProcedure [dbo].[SP_ProductById]    Script Date: 5/7/2023 10:45:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_ProductById] 
	-- Add the parameters for the stored procedure here
	@Id bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Get statements for procedure here
	SELECT * from dbo.Products where Id = @Id
END