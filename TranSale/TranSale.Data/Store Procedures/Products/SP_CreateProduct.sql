USE [SaleTransactionLat]
GO
/****** Object:  StoredProcedure [dbo].[SP_CreateProduct]    Script Date: 5/7/2023 6:10:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_CreateProduct]
	-- Add the parameters for the stored procedure here
	@Name [nvarchar](max),
	@BrandName [nvarchar](max),
	@Price int,
	@Description [nvarchar](max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into dbo.Products
	(
		Name,
		BrandName,
		Price,
		Description,
		CreationDate
	)
	values
	(
		@Name,
		@BrandName,
		@Price,
		@Description,
		GETDATE()
	)
END