-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateSaleTransactionForInvoice]
	-- Add the parameters for the stored procedure here
	@Id bigint,
	@CustomerId bigint,
	@InvoiceId bigint,
	@ProductId bigint,
	@Rate decimal,
	@Quantity decimal,
	@TotalAmount decimal
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- update statements for procedure here
	update dbo.SaleTransactions
	set
		CustomerId = @CustomerId,
		InvoiceId = @InvoiceId,
		ProductId = @ProductId,
		Rate = @Rate,
		Quantity = @Quantity,
		TotalAmount = @TotalAmount,
		ModifiedDate = GETDATE()
	where Id = @Id
END
