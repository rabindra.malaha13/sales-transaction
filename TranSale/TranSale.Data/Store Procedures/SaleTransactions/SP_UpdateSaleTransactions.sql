USE [SaleTransaction]
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateSaleTransactions]    Script Date: 5/12/2023 10:45:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_UpdateSaleTransactions]
	-- Add the parameters for the stored procedure here
	@Id bigint,
	@CustomerId bigint,
	@InvoiceId bigint,
	@ProductId bigint,
	@Rate decimal,
	@Quantity decimal,
	@TotalAmount decimal
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- update statements for procedure here
	update dbo.SaleTransactions
	set
		CustomerId = @CustomerId,
		InvoiceId = @InvoiceId,
		ProductId = @ProductId,
		Rate = @Rate,
		Quantity = @Quantity,
		TotalAmount = @TotalAmount,
		ModifiedDate = GETDATE()
	where Id = @Id
END
