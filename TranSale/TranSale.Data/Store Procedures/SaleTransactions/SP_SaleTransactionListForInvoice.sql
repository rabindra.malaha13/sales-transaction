-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_SaleTransactionListForInvoice]
	@InvoiceId bigint,
	@CustomerId bigint
AS
BEGIN
	SELECT sale.Id, CustomerId, ProductId, Rate, Quantity, TotalAmount, InvoiceId, customer.Name as CustomerName, product.Name as ProductName
	FROM dbo.SaleTransactions sale Inner join 
	dbo.Customers customer on sale.CustomerId = customer.Id
	inner join dbo.Products product on sale.ProductId = product.Id
	where sale.CustomerId = @CustomerId and sale.InvoiceId = @InvoiceId
	order by customer.Name  
END
