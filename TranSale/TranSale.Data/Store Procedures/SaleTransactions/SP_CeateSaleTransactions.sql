-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[SP_CeateSaleTransactions] 
    -- Add the parameters for the stored procedure here
    @CustomerId bigint,
	@ProductId bigint,
	@Rate decimal,
	@Quantity decimal,
	@TotalAmount decimal
AS
BEGIN
    insert into dbo.SaleTransactions
	(
		CustomerId,
		ProductId,
		Rate,
		Quantity,
		TotalAmount,
		CreationDate
	)
	values
	(
		@CustomerId,
		@ProductId,
		@Rate,
		@Quantity,
		@TotalAmount,
		GETDATE()
	)
END
