USE [SaleTransaction]
GO
/****** Object:  StoredProcedure [dbo].[SP_SaleTransactionList]    Script Date: 5/12/2023 11:13:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_SaleTransactionList]
AS
BEGIN
	SELECT sale.Id, CustomerId, ProductId, Rate, Quantity, TotalAmount, InvoiceId, customer.Name as CustomerName, product.Name as ProductName
	FROM dbo.SaleTransactions sale Inner join 
	dbo.Customers customer on sale.CustomerId = customer.Id
	inner join dbo.Products product on sale.ProductId = product.Id
	order by customer.Name
END