﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Domain.Customers;
using TranSale.Domain.Invoices;
using TranSale.Domain.Products;
using TranSale.Domain.SaleTransactions;

namespace TranSale.Data.AppDbContext
{
    public class TranSaleDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public TranSaleDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("TranSaleConnection"));
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SaleTransaction> SaleTransactions { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }

        //Fake Class
        public virtual DbSet<SaleTransactionViewModel> SaleTransactionViewModels { get; set; }
        public virtual DbSet<InvoiceReturn> InvoiceReturns { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)// Crée la migration
        {
            modelBuilder.Entity<Product>().Property(p => p.Price).HasColumnType("decimal(18,4)");

            modelBuilder.Entity<SaleTransaction>(b =>
            {
                b.HasOne<Product>().WithMany().HasForeignKey(x => x.ProductId).IsRequired();
                b.HasOne<Customer>().WithMany().HasForeignKey(x => x.CustomerId).IsRequired();
                b.HasOne<Invoice>().WithMany().HasForeignKey(x => x.InvoiceId);
                b.Property(p => p.Rate).HasColumnType("decimal(18,4)");
                b.Property(p => p.Quantity).HasColumnType("decimal(18,4)");
                b.Property(p => p.TotalAmount).HasColumnType("decimal(18,4)");
            });

            modelBuilder.Entity<Invoice>(b =>
            {
                b.HasOne<Customer>().WithMany().HasForeignKey(x => x.CustomerId).IsRequired();
                b.Property(p => p.TotalAmount).HasColumnType("decimal(18,4)");
            });
        }
    }
}
