﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.Products
{
    public class Product : BaseEntity
    {
        public string? Name { get; set; }
        public string? BrandName { get; set; }
        public decimal? Price { get; set; }
        public string? Description { get; set; }

        public Product(string? name, string? brandName, decimal? price, string? description)
        {
            Name = name;
            BrandName = brandName;
            Price = price;
            Description = description;
        }
    }
}
