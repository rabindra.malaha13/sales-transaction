﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.SaleTransactions
{
    [Keyless]
    public class SaleTransactionViewModel
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public long? InvoiceId { get; set; }
        public string? CustomerName { get; set; }
        public string? ProductName { get; set; }
        public decimal Rate { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
