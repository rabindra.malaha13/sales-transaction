﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.SaleTransactions
{
    public class SaleTransaction : BaseEntity
    {
        [ForeignKey("CustomerId")]
        public long CustomerId { get; set; }

        [ForeignKey("ProductId")]
        public long ProductId { get; set; }
        public decimal Rate { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalAmount { get; set; }

        [ForeignKey("InvoiceId")]
        public long? InvoiceId { get; set; }

        public SaleTransaction(long customerId, long productId, decimal rate, decimal quantity, decimal totalAmount)
        {
            CustomerId = customerId;
            ProductId = productId;
            Rate = rate;
            Quantity = quantity;
            TotalAmount = totalAmount;
        }
    }
}
