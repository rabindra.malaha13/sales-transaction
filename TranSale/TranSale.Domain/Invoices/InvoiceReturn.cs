﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.Invoices
{
    [Keyless]
    public class InvoiceReturn
    {
        public decimal ReturnId { get; set; }
    }
}
