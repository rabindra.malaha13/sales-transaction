﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.Invoices
{
    public class Invoice : BaseEntity
    {
        [ForeignKey("CustomerId")]
        public long CustomerId { get; set; }
        public decimal TotalAmount { get; set; }
        public string? Remarks { get; set; }
    }
}
