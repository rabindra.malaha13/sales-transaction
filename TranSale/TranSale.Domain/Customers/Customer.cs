﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Domain.Customers
{
    public class Customer : BaseEntity
    {
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? ContactNumber { get; set; }
        public string? Address { get; set; }

        public Customer(string? name, string? email, string? contactNumber, string? address)
        {
            Name = name;
            Email = email;
            ContactNumber = contactNumber;
            Address = address;
        }
    }
}
