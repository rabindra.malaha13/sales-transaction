﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TranSale.Application.Infrastructure.Products;

namespace TranSale.Api.Controllers.Products
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : ControllerBase, IProductService
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost]
        public async Task<int> CreateAsync(ProductCreateDto input)
        {
            return await _productService.CreateAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(long id)
        {
            await _productService.DeleteAsync(id);
        }

        [HttpGet]
        public async Task<List<ProductDto>> GetAllProductAsync()
        {
            return await _productService.GetAllProductAsync();
        }

        [HttpGet("get-select-items")]
        public async Task<List<ProductSelectModelDto>> GetAllProductSelectModelAsync()
        {
            return await _productService.GetAllProductSelectModelAsync();
        }

        [HttpGet("{id}")]
        public async Task<ProductDto> GetProductByIdAsync(long id)
        {
            return await _productService.GetProductByIdAsync(id);
        }

        [HttpPut("{id}")]
        public async Task<int> UpdateAsync(long id, ProductUpdateDto input)
        {
            return await _productService.UpdateAsync(id, input);
        }
    }
}
