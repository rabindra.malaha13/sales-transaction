﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TranSale.Application.Infrastructure.Customers;

namespace TranSale.Api.Controllers.Customers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase, ICustomerService
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost]
        public async Task<int> CreateCustomerAsync(CustomerCreateDto input)
        {
            return await _customerService.CreateCustomerAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(long id)
        {
            await _customerService.DeleteAsync(id);
        }

        [HttpGet]
        public async Task<List<CustomerDto>> GetAllCustomerAsync()
        {
            return await _customerService.GetAllCustomerAsync();
        }

        [HttpGet("get-select-items")]
        public async Task<List<CustomerSelectModelDto>> GetAllCustomerSelectModelAsync()
        {
            return await _customerService.GetAllCustomerSelectModelAsync();
        }

        [HttpGet("{id}")]
        public async Task<CustomerDto> GetCustomerByIdAsync(long id)
        {
            return await _customerService.GetCustomerByIdAsync(id);
        }

        [HttpPut("{id}")]
        public async Task<int> UpdateCustomerAsync(long id, CustomerUpdateDto input)
        {
            return await _customerService.UpdateCustomerAsync(id, input);
        }
    }
}
