﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TranSale.Application.Infrastructure.Invoices;
using TranSale.Application.Infrastructure.SaleTransactions;

namespace TranSale.Api.Controllers.SaleTransactions
{
    [Route("api/sale-transactions")]
    [ApiController]
    public class SaleTransactionController : ControllerBase, ISaleTransactionService
    {
        private readonly ISaleTransactionService _saleTransactionService;
        public SaleTransactionController(ISaleTransactionService saleTransactionService)
        {
            _saleTransactionService = saleTransactionService;
        }

        [HttpPost]
        public async Task<int> CreateSaleTransactionAsync(SaleTransactionCreateDto input)
        {
            return await _saleTransactionService.CreateSaleTransactionAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task DeleteAsync(long id)
        {
            await _saleTransactionService.DeleteAsync(id);
        }

        [HttpPost("generate-invoice")]
        public int GenerateInvoice(InvoiceCreateDto input)
        {
            return _saleTransactionService.GenerateInvoice(input);
        }

        [HttpGet]
        public async Task<List<SaleTransactionDto>> GetAllSaleTransactionAsync()
        {
            return await _saleTransactionService.GetAllSaleTransactionAsync();
        }

        [HttpGet("get-sale-transaction-for-invoice/{invoiceId}/{customerId}")]
        public async Task<List<SaleTransactionDto>> GetAllSaleTransactionByInvoiceIdAndCustomerIdAsync(long invoiceId, long customerId)
        {
            return await _saleTransactionService.GetAllSaleTransactionByInvoiceIdAndCustomerIdAsync(invoiceId, customerId);
        }

        [HttpGet("{id}")]
        public async Task<SaleTransactionDto> GetSaleTransactionByIdAsync(long id)
        {
            return await _saleTransactionService.GetSaleTransactionByIdAsync(id);
        }

        [HttpPut("{id}")]
        public  int UpdateSaleTransactionAsync(long id, SaleTransactionUpdateDto input)
        {
            return _saleTransactionService.UpdateSaleTransactionAsync(id, input);
        }
    }
}
