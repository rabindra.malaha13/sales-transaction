﻿using AutoMapper;
using TranSale.Application.Implementation;
using TranSale.Application.Implementation.Customers;
using TranSale.Application.Implementation.Products;
using TranSale.Application.Implementation.SaleTransactions;
using TranSale.Application.Infrastructure.Customers;
using TranSale.Application.Infrastructure.Products;
using TranSale.Application.Infrastructure.SaleTransactions;
using TranSale.Data.AppDbContext;

namespace TranSale.Api.Configurations
{
    public static class TranSaleConfiguration
    {
        public static void ConfigureAppServices(WebApplicationBuilder builder)
        {
            //Automapper configuration
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperModule());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            builder.Services.AddSingleton(mapper);

            //Db context
            builder.Services.AddDbContext<TranSaleDbContext>(ServiceLifetime.Transient);

            //Service Injection
            builder.Services.AddScoped<ICustomerService, CustomerService>();
            builder.Services.AddScoped<IProductService, ProductService>();
            builder.Services.AddScoped<ISaleTransactionService, SaleTransactionService>();
        }
    }
}
