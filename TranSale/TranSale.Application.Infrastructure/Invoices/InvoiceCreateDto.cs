﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.SaleTransactions;

namespace TranSale.Application.Infrastructure.Invoices
{
    public class InvoiceCreateDto
    {
        public long CustomerId { get; set; }
        public decimal TotalAmount { get; set; }
        public List<SaleTransactionDto> SaleTransactions { get; set; }
    }
}
