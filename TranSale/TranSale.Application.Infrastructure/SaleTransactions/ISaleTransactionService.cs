﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.Invoices;

namespace TranSale.Application.Infrastructure.SaleTransactions
{
    public interface ISaleTransactionService
    {
        Task<List<SaleTransactionDto>> GetAllSaleTransactionAsync();
        Task<List<SaleTransactionDto>> GetAllSaleTransactionByInvoiceIdAndCustomerIdAsync(long invoiceId, long customerId);
        Task<SaleTransactionDto> GetSaleTransactionByIdAsync(long id);
        Task<int> CreateSaleTransactionAsync(SaleTransactionCreateDto input);
        int UpdateSaleTransactionAsync(long id, SaleTransactionUpdateDto input);
        int GenerateInvoice(InvoiceCreateDto input);
        Task DeleteAsync(long id);
    }
}
