﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.SaleTransactions
{
    public class SaleTransactionUpdateDto : BaseDto
    {
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        public long InvoiceId { get; set; }
        public decimal Rate { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
