﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.Products
{
    public class ProductSelectModelDto
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public decimal Price { get; set; }
    }
}
