﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.Products
{
    public class ProductDto
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? BrandName { get; set; }
        public Decimal? Price { get; set; }
        public string? Description { get; set; }
    }
}
