﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.Products
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetAllProductAsync();
        Task<ProductDto> GetProductByIdAsync(long id);
        Task<int> CreateAsync(ProductCreateDto input);
        Task<int> UpdateAsync(long id, ProductUpdateDto input);
        Task<List<ProductSelectModelDto>> GetAllProductSelectModelAsync();
        Task DeleteAsync(long id);
    }
}
