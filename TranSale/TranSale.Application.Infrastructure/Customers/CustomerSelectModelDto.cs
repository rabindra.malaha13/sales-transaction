﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.Customers
{
    public class CustomerSelectModelDto
    {
        public long Id { get; set; }
        public string? Name { get; set; }
    }
}
