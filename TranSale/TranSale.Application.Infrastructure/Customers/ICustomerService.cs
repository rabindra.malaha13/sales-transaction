﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSale.Application.Infrastructure.Customers
{
    public interface ICustomerService
    {
        Task<List<CustomerDto>> GetAllCustomerAsync();
        Task<CustomerDto> GetCustomerByIdAsync(long id);
        Task<int> CreateCustomerAsync(CustomerCreateDto input);      
        Task<int> UpdateCustomerAsync(long id, CustomerUpdateDto input);
        Task<List<CustomerSelectModelDto>> GetAllCustomerSelectModelAsync();
        Task DeleteAsync(long id);
    }
}
