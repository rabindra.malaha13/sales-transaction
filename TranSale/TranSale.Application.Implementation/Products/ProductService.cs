﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.Products;
using TranSale.Data.AppDbContext;
using TranSale.Domain.Products;

namespace TranSale.Application.Implementation.Products
{
    public class ProductService : IProductService
    {
        private readonly TranSaleDbContext _dbContext;
        private readonly IMapper _mapper;
        public ProductService(TranSaleDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        public async Task<int> CreateAsync(ProductCreateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Name", input.Name));
            parameter.Add(new SqlParameter("@BrandName", input.BrandName));
            parameter.Add(new SqlParameter("@Price", input.Price));
            parameter.Add(new SqlParameter("@Description", input.Description));

            var result = await Task.Run(() =>
            _dbContext.Database.ExecuteSqlRawAsync(@"exec SP_CreateProduct @Name, @BrandName, @Price, @Description", parameter.ToArray()));
            return result;
        }

        public async Task DeleteAsync(long id)
        {
            await Task.Run(() => _dbContext.Database.ExecuteSqlInterpolatedAsync($"SP_DeleteProduct {id}"));
        }

        public async Task<List<ProductDto>> GetAllProductAsync()
        {
            var products = await _dbContext.Products.FromSqlRaw<Product>("SP_PrductList").ToListAsync();
            return _mapper.Map<List<Product>, List<ProductDto>>(products);
        }

        public async Task<List<ProductSelectModelDto>> GetAllProductSelectModelAsync()
        {
            var selectProducts = await _dbContext.Products.FromSqlRaw("SP_ProductSelectOption").ToListAsync();
            return _mapper.Map<List<ProductSelectModelDto>>(selectProducts);
        }

        public async Task<ProductDto> GetProductByIdAsync(long id)
        {
            var param = new SqlParameter("@Id", id);
            var product = await Task.Run(() => _dbContext.Products.FromSqlRaw(@"exec SP_ProductById @Id", param).ToListAsync());
            return _mapper.Map<ProductDto>(product.FirstOrDefault());
        }

        public async Task<int> UpdateAsync(long id, ProductUpdateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Id", id));
            parameter.Add(new SqlParameter("@Name", input.Name));
            parameter.Add(new SqlParameter("@BrandName", input.BrandName));
            parameter.Add(new SqlParameter("@Price", input.Price));
            parameter.Add(new SqlParameter("@Description", input.Description));

            var result = await Task.Run(() =>
            _dbContext.Database.ExecuteSqlRawAsync(@"exec SP_UpdateProduct @Id, @Name, @BrandName, @Price, @Description", parameter.ToArray()));
            return result;
        }
    }
}
