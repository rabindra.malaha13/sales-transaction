﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.Invoices;
using TranSale.Application.Infrastructure.SaleTransactions;
using TranSale.Data.AppDbContext;
using TranSale.Domain.Invoices;
using TranSale.Domain.SaleTransactions;

namespace TranSale.Application.Implementation.SaleTransactions
{
    public class SaleTransactionService : ISaleTransactionService
    {
        private readonly TranSaleDbContext _dbContext;
        private readonly IMapper _mapper;
        public SaleTransactionService(TranSaleDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<int> CreateSaleTransactionAsync(SaleTransactionCreateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@CustomerId", input.CustomerId));
            parameter.Add(new SqlParameter("@ProductId", input.ProductId));
            parameter.Add(new SqlParameter("@Rate", input.Rate));
            parameter.Add(new SqlParameter("@Quantity", input.Quantity));
            parameter.Add(new SqlParameter("@TotalAmount", input.TotalAmount));

            var result = await Task.Run(() =>
            _dbContext.Database.ExecuteSqlRawAsync(@"exec SP_CeateSaleTransactions @CustomerId, @ProductId, @Rate, @Quantity, @TotalAmount", parameter.ToArray()));
            return result;
        }

        public async Task DeleteAsync(long id)
        {
            await Task.Run(() => _dbContext.Database.ExecuteSqlInterpolatedAsync($"Delete SP_SaleTransaction {id}"));
        }

        public int GenerateInvoice(InvoiceCreateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@CustomerId", input.CustomerId));
            parameter.Add(new SqlParameter("@TotalAmount", input.TotalAmount));

            var result =  _dbContext.InvoiceReturns.FromSqlRaw<InvoiceReturn>(@"exec SP_CreateInvoice @CustomerId, @TotalAmount", parameter.ToArray()).ToList();
            var invoiceId = result.First().ReturnId;

            var sales = _mapper.Map<List<SaleTransactionUpdateDto>>(input.SaleTransactions);
            sales.ForEach(x =>
            {
                x.InvoiceId = Convert.ToInt64(invoiceId);
                UpdateSaleTransactionForInvoiceAsync(x.Id, x);
            });
            return 1;
        }

        public async Task<List<SaleTransactionDto>> GetAllSaleTransactionAsync()
        {
            var saleTransactions = await _dbContext.SaleTransactionViewModels.FromSqlRaw<SaleTransactionViewModel>($"SP_SaleTransactionList").ToListAsync();
            return _mapper.Map<List<SaleTransactionDto>>(saleTransactions);
        }

        public async Task<List<SaleTransactionDto>> GetAllSaleTransactionByInvoiceIdAndCustomerIdAsync(long invoiceId, long customerId)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@CustomerId", customerId));
            parameter.Add(new SqlParameter("@InvoiceId", invoiceId));
            var saleTransactions = await _dbContext.SaleTransactionViewModels.FromSqlRaw<SaleTransactionViewModel>(@"exec SP_SaleTransactionListForInvoice @InvoiceId, @CustomerId", parameter.ToArray()).ToListAsync();
            return _mapper.Map<List<SaleTransactionDto>>(saleTransactions);
        }

        public async Task<SaleTransactionDto> GetSaleTransactionByIdAsync(long id)
        {
            var param = new SqlParameter("@Id", id);
            var saleTransactions = await Task.Run(() => _dbContext.SaleTransactions.FromSqlRaw(@"exec SP_SaleTransactionById @Id", param).ToListAsync());
            return _mapper.Map<SaleTransactionDto>(saleTransactions.FirstOrDefault());
        }

        public int UpdateSaleTransactionAsync(long id, SaleTransactionUpdateDto input)
        {
            int result = 0;
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Id", id));
            parameter.Add(new SqlParameter("@CustomerId", input.CustomerId));
            parameter.Add(new SqlParameter("@ProductId", input.ProductId));
            parameter.Add(new SqlParameter("@Rate", input.Rate));
            parameter.Add(new SqlParameter("@Quantity", input.Quantity));
            parameter.Add(new SqlParameter("@TotalAmount", input.TotalAmount));

            result = _dbContext.Database.ExecuteSqlRaw(@"exec SP_UpdateSaleTransactions @Id, @CustomerId, @ProductId, @Rate, @Quantity, @TotalAmount", parameter.ToArray());
            return result;
        }

        public int UpdateSaleTransactionForInvoiceAsync(long id, SaleTransactionUpdateDto input)
        {
            int result = 0;
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Id", id));
            parameter.Add(new SqlParameter("@CustomerId", input.CustomerId));
            parameter.Add(new SqlParameter("@ProductId", input.ProductId));
            parameter.Add(new SqlParameter("@Rate", input.Rate));
            parameter.Add(new SqlParameter("@Quantity", input.Quantity));
            parameter.Add(new SqlParameter("@TotalAmount", input.TotalAmount));
            parameter.Add(new SqlParameter("@InvoiceId", input.InvoiceId));

            result = _dbContext.Database.ExecuteSqlRaw(@"exec SP_UpdateSaleTransactionForInvoice @Id, @CustomerId, @InvoiceId, @ProductId, @Rate, @Quantity, @TotalAmount", parameter.ToArray());
            return result;
        }
    }
}
