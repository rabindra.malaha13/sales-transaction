﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.Customers;
using TranSale.Data.AppDbContext;
using TranSale.Domain.Customers;

namespace TranSale.Application.Implementation.Customers
{
    public class CustomerService : ICustomerService
    {
        private readonly TranSaleDbContext _dbContext;
        private readonly IMapper _mapper;
        public CustomerService(TranSaleDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<int> CreateCustomerAsync(CustomerCreateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Name", input.Name));
            parameter.Add(new SqlParameter("@Email", input.Email));
            parameter.Add(new SqlParameter("@ContactNumber", input.ContactNumber));
            parameter.Add(new SqlParameter("@Address", input.Address));

            var result = await Task.Run(() =>
                _dbContext.Database.ExecuteSqlRawAsync(@"exec SP_CreateCustomer @Name, @Email, @ContactNumber, @Address", parameter.ToArray()));
            return result;
        }

        public async Task DeleteAsync(long id)
        {
            await Task.Run(() => _dbContext.Database.ExecuteSqlInterpolatedAsync($"SP_DeleteCustomer {id}"));
        }

        public async Task<List<CustomerDto>> GetAllCustomerAsync()
        {
            var customers = await _dbContext.Customers.FromSqlRaw<Customer>("SP_CustomerList").ToListAsync();
            var dtos = _mapper.Map<List<CustomerDto>>(customers);
            return _mapper.Map<List<CustomerDto>>(customers);
        }

        public async Task<List<CustomerSelectModelDto>> GetAllCustomerSelectModelAsync()
        {
            var selectValues = await _dbContext.Customers.FromSqlRaw("SP_CustomerSelectOption").ToListAsync();
            return _mapper.Map<List<CustomerSelectModelDto>>(selectValues);
        }

        public async Task<CustomerDto> GetCustomerByIdAsync(long id)
        {
            var param = new SqlParameter("@Id", id);
            var customer = await Task.Run(() => _dbContext.Customers.FromSqlRaw(@"exec SP_CustomerById @Id", param).ToListAsync());
            return _mapper.Map<CustomerDto>(customer.FirstOrDefault());
        }

        public async Task<int> UpdateCustomerAsync(long id, CustomerUpdateDto input)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Id", id));
            parameter.Add(new SqlParameter("@Name", input.Name));
            parameter.Add(new SqlParameter("@Email", input.Email));
            parameter.Add(new SqlParameter("@ContactNumber", input.ContactNumber));
            parameter.Add(new SqlParameter("@Address", input.Address));

            var result = await Task.Run(() =>
                _dbContext.Database.ExecuteSqlRawAsync(@"exec SP_UpdateCustomer @Id, @Name, @Email, @ContactNumber, @Address", parameter.ToArray()));
            return result;
        }
    }
}
