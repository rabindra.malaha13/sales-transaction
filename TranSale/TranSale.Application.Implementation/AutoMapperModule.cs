﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSale.Application.Infrastructure.Customers;
using TranSale.Application.Infrastructure.Products;
using TranSale.Application.Infrastructure.SaleTransactions;
using TranSale.Domain.Customers;
using TranSale.Domain.Products;
using TranSale.Domain.SaleTransactions;

namespace TranSale.Application.Implementation
{
    public class AutoMapperModule : Profile
    {
        public AutoMapperModule()
        {
            CreateMap<Customer, CustomerDto>().ReverseMap();
            CreateMap<Customer, CustomerCreateDto>().ReverseMap();
            CreateMap<Customer, CustomerUpdateDto>().ReverseMap();
            CreateMap<Customer, CustomerSelectModelDto>().ReverseMap();

            CreateMap<Product, ProductDto>().ReverseMap();
            CreateMap<Product, ProductCreateDto>().ReverseMap();
            CreateMap<Product, ProductUpdateDto>().ReverseMap();
            CreateMap<Product, ProductSelectModelDto>().ReverseMap();

            CreateMap<SaleTransaction, SaleTransactionDto>().ReverseMap();
            CreateMap<SaleTransaction, SaleTransactionCreateDto>().ReverseMap();
            CreateMap<SaleTransaction, SaleTransactionUpdateDto>().ReverseMap();
            CreateMap<SaleTransactionDto, SaleTransactionUpdateDto>().ReverseMap();
            CreateMap<SaleTransactionDto, SaleTransactionViewModel>().ReverseMap();
        }
    }
}
